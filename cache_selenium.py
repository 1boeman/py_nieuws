#!/bin/env python
import os
import time
import importlib
import helpers.util as u 
from selenium import webdriver


logger = u.get_logger(__file__)


def main():
    _dir = u.get_dir_setting('selenium_dir')
    u.delete_dir_contents(_dir, max_file_age = 60 * 60 * 24 * 2)
    _file = u.get_setting('selenium_file')

    if not os.path.exists(_file):
      print (_file, ' was not found.')
      return

    options = webdriver.FirefoxOptions() 
    #options.headless = True
    options.add_argument('-headless')
    driver = webdriver.Firefox(options=options) 


#    driver = get_chromedriver()
    #except:
    #   driver = get_geckodriver()

    lines = u.get_file_lines(_file)
    for line in lines:
        try:
            print(line)
            driver.get(line)
            time.sleep(8)
            file_name = u.url_to_filename(line)
            file_path = os.path.join(_dir, file_name)
            with open (file_path, "w") as g:
                print (driver.page_source, file = g)
        except Exception as e:
            logger.exception(e)
            continue
    driver.quit()

#
#def get_chromedriver():
#    from selenium.webdriver.chrome.service import Service as ChromeService
#    from webdriver_manager.chrome import ChromeDriverManager
#    from selenium.webdriver.chrome.options import Options
#
##    PROXY = "socks5://localhost:9050" # IP:PORT or HOST:PORT$
#
#
#    options = Options()
#
##    options.add_argument('--proxy-server=%s' % PROXY)
#    options.add_argument('--headless=new')
#    options.add_argument('--disable-extensions')
#    options.add_argument('--disable-gpu')
#    options.add_argument('--no-sandbox')
#    options.add_argument('--disable-dev-shm-usage')
#    driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options)
#    return driver
#
#def get_geckodriver():
#    from selenium.webdriver.firefox.service import Service
#    from selenium.webdriver.firefox.options import Options
#
#    options = Options()
#    options.add_argument("--headless")
#    s = Service('./geckodriver')
#    driver = webdriver.Firefox(service=s, options=options)
#    return driver
#
if __name__ == "__main__":
    main()
