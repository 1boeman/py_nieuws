#!/bin/env bash

cd "$(dirname "$0")"
#!/bin/bash

if [ -n "$1" ]
then
    export "PYNIEUWS_CONFIG"=$1
    echo "using $PYNIEUWS_CONFIG for config file"
else
     echo "Specify path to config file."
     exit 1
fi

python parse_pages.py
python cache_requests.py
python cache_selenium.py
python make_data.py
python render_pages.py


