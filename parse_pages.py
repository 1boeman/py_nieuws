#!/bin/env python
import sqlite3 as sql3
from helpers.util import get_setting, get_dir_setting
import helpers.util as u
import click
import traceback
import os


logger = u.get_logger(__file__)


sources = get_dir_setting('sources_dir')
feeds_file = get_setting('feeds_file')
sites_file = get_setting('sites_file')
selenium_file = get_setting('selenium_file')
json_file = get_setting('json_file')
db_file = get_setting('db_file')


def main():
    files = [feeds_file, sites_file, selenium_file, db_file, json_file]
    [delete_file(file) for file in files]

    conn = sql3.connect(db_file)
    create_db(conn)
    pages = u.get_pages()
    for p in pages:
        if pages[p]['type'] == 'news':
            logger.info("Processing news page: " + pages[p]['title'])
            news_page(pages[p], conn)
    conn.close()


def news_page(page_yaml, conn):
    for section in page_yaml['content']:
        for entry in section:
            register_source(entry)
            register_source_db(entry, conn)


def delete_file(path):
    try:
        os.remove(path)
    except OSError:
        pass


def create_db(conn):
    conn.execute('''CREATE TABLE sources
           (id INTEGER PRIMARY KEY   NOT NULL,
            title           TEXT  NOT NULL,
            src             TEXT  NOT NULL,
            url             TEXT  NOT NULL,
            type            TEXT  NOT NULL,
            process         TEXT,
            data_file       TEXT  NOT NULL UNIQUE);''')


def register_source_db(data, conn):
    process = data.get('process', None)
    row = [data['title'], data['src'], data['url'], data['type'], process]
    row.append(u.url_to_filename(data['src']))
    sql = ''' insert into sources (title,src,url,type,process,data_file)
                values(?,?,?,?,?,?)'''
    cur = conn.cursor()
    try:
        cur.execute(sql, row)
        conn.commit()
    except Exception as e:
        logger.warning(e)
        logger.warning(row)
        return 0
    return cur.lastrowid


def register_source(data):
    if 'skip' in data:
        return
    if data["type"] =='feed':
        file_path = feeds_file
    elif data["type"] == 'html':
        file_path = sites_file
    elif data["type"] == 'selenium':
        file_path = selenium_file
    elif data["type"] == 'json':
        file_path = json_file
    else:
        raise Exception("data type unknown: " + data["type"])

    with open(file_path, 'a+') as fw:
        fw.write(f'\n{data["src"]}')


if __name__ == "__main__":
    main()
