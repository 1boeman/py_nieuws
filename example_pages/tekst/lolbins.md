title: LOLbins
date: 09/07/2023
----


## Wat zijn LOLBins

De LOL binnen Lolbins is een afkorting voor "Living Of the Land".<br> 
LOLBins zijn binaries die standaard op het Operating System aanwezig zijn, en die door aanvallers misbruikt zijn bij het aanvallen van een systeem.


Deze exploits kunnen dus worden ingezet nadat de aanvaller zich toegang heeft verschaft tot een systeem.


Het [LOLBAS project](https://lolbas-project.github.io/) definieert LOLbins alsvolgt:


  -    Be a *Microsoft-signed* file, either native to the OS or downloaded from *Microsoft*.
  -    Have extra "unexpected" functionality. It is not interesting to document intended use cases.<br>
        Exceptions are application whitelisting bypasses
  -    Have functionality that would be useful to an APT or red team


**LOLBAS** onderhoudt een lijst van misbruikbare binaries op Windows/Microsoft besturingssystemen.


  - [https://lolbas-project.github.io/](https://lolbas-project.github.io/) 



Zie **GTFOBins** voor een lijst van exploits van binaries op Linux:


 - [https://gtfobins.github.io/](https://gtfobins.github.io/)  - *"a curated list of Unix binaries that can be used to bypass local security restrictions in misconfigured systems"*


En voor macOS:


-  [https://www.loobins.io/](https://www.loobins.io/)
