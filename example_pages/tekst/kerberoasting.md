title: Kerberos
date: 08/07/2023
----

Het authenticatie protocol Kerberos werd oorspronkelijk eind jaren tachtig  ontwikkeld op 't **Massachusetts Institute of Technology** (MIT).


Binnen het client-server model van Kerberos bestaat het authenticatie proces uit de volgende stappen:


1. Client stuurt een bericht met de user ID naar 't *"Key Distribution Center"* (KDC). 't wachtwoord van de client wordt hierbij niet verzonden. Het Key Distribution Center is een service welke draait op de Domain Controllers van een domein. De KDC service bestaat uit twee onderdelen:
    - Ticket-Granting Service (TGS) welke tickets uitgeeft voor connecties met computers binnen 't domein. 
    - Authenticatie Service (AS) welke *"ticket-granting tickets"* (TGTs) uitgeeft. Nog voordat een client een ticket voor een andere computer kan opvragen bij de TGS moet eerst een *TGT* worden opgevraagd bij de Authenticatie Service. 
2. De authenticatie service zoekt vervolgens de gebruiker in z'n database (*Active Directory* bij Windows servers), en genereert daarna een **"Ticket Granting Ticket"** (TGT) met timestamp (welke de geldigheidsduur bepaalt) geëncrypteerd op basis van de *TGS secret key*. <br> De Authenticatie Service verstuurt vervolgens naar de client:
    - een session key *"SK1"* - geëncrypt op basis van de secret key van de client/gebruiker
    - Ticket Granting Ticket (TGT) bevattende client ID, client netwerk adres, ticket timestamp/geldigheidsduur en de SK1, allemaal geëncrypt met de secret key van de TGS.
3. De client gebruikt de secret key van de gebruiker om de SK1 en TGT uit het antwoord van de KDC te abstraheren (wanneer de sessie (SK1) verloopt vraagt de client om een nieuwe TGT/Sessie).
4. De client vraagt nu met TGT aan de TGS om toegang tot een andere server op het netwerk en specificeert hierbij de *Service Principal Name* (SPN) van de Server tot welke toegang wordt aangevraagd.
6. De KDC valideert de TGT en stuurt een valide service ticket voor de Server met de gespecificeerde SPN.
7. De client presenteert nu de door de TGS gegenereerde service ticket aan de server met SPN en krijgt op basis daarvan toegang.


---
## Aanvallen op Kerberos
### Kerberoasting
Kerberoasting is een aanvalstechniek gericht op het kraken van wachtwoorden van accounts met een "Service Principal Name" in een Microsoft Active Directory gebaseerd netwerk. <br>
Een aanvaller ingelogd op een domein vraagt een service ticket op voor een server met een *Service Principal Name*. De ticket die wordt uitgegeven is standaard ge-encrypt met een hash van het service account dat bij de betreffende *Service Principal Name* hoort. De aanvaller probeert vervolgens offline via Brute Force het bijbehorende wachtwoord te decrypten.<br>
Zodra dat lukt kan de aanvaller inloggen / tickets aanvragen met de credentials van het gecompromiteerde service account, en dus gebruik maken van de bijbehorende rechten op het netwerk.

### Pass the ticket
Bij een "pass the ticket" aanval wordt er geauthenticeerd als gebruiker zonder diens wachtwoord te kennen, door gebruik te maken van "gestolen" geldige tickets aanwezig op een gecompromiteerd systeem. Afhankelijk van de rechten die men op het systeem heeft kunnen bemachtigen, kunnen dit zowel service tickets als ticket granting tickets zijn. TGT's kunnen weer misbruikt worden om nieuwe service tickets op te vragen.<br>
Bijv. *Mimikatz* kan gebruikt worden om zowel Kerberos tickets te abstraheren uit het actieve geheugen gebruikt door *LSASS.exe*, als deze te injecteren in een sessie van de aanvaller. 


---
#### termen:
- *krbtgt*<br>
De security principal name (SPN) van de KDC is **krbtgt**
- De drie *"koppen" van Kerberos* zijn:  
    1. De authentiecatie server (AS)
    2. De ticket granting server (TGS)
    3. De database met gebruiker ID's en wachtwoorden (*Active Directory* bij Microsoft)


