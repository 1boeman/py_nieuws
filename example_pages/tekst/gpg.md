title: Simpele GPG encrypt/decrypt van bestand met wachtwoord
date: 08/09/2023
----


Gebruik GPG om een bestand met wachtwoord te encrypten (symmetrisch):


```
gpg -c ./test.txt
```


(Wachtwoord prompt)


```
rm ./test.txt
```


Gebruik GPG (in script) om weer te decrypten:


```
gpg --batch --yes -d -o test.txt --passphrase "testing" test.txt.gpg
```
