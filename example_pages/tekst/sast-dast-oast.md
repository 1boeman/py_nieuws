title: SAST, DAST, OAST, IAST
date: 18/07/2023
----


##  SAST
**Static Application Security Testing** is geautomatiseerde analyse van broncode als onderdeel van het software ontwikkelproces.
Staat ook wel bekend als "white box testing" omdat de applicatie getest wordt op basis van kennis van de innerlijke structuur ervan. 


Voorbeelden:
    - [Gitlab SAST](https://docs.gitlab.com/ee/user/application_security/sast/)


##  DAST
**Dynamic Application Security Testing** is het testen van een applicatie via de toegang die deze van buitenaf biedt, evt. zonder dat kennis van de architectuur ervan daarbij noodzakelijk is. AKA Black box testing. 


##  OAST
**Out-of-band Application Security Testing** is een uitbreiding op DAST, waarbij de aanval erop gericht is de aangevallen dienst te laten interacteren met een (door de aanvaller gekozen) derde systeem, dat zich buiten het doelwit domein bevindt.


##  IAST
**Interactive Application Security Testing** is het observeren/analyseren van de werking van de code van een applicatie terwijl deze in bedrijf is, d.m.v. een geautomatiseerde test, een menselijke tester, of een andere activiteit waarin direct geinteracteerd wordt met de functionaliteit die de applicatie biedt.
