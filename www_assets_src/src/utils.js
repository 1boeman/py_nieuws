const q = function(selector){
    let items = document.querySelectorAll(selector);
    return [...items]
};

const parents = function(el, selector) {
  const parents = [];
  while ((el = el.parentNode) && el !== document) {
    if (!selector || el.matches(selector)) parents.push(el);
  }
  return parents;
}

const clck = function(el,callback){
    el.addEventListener('click',function(e){
        e.stopPropagation()
        callback.apply(this,[e]); 
    }); 
};


const ready = function(fn) {
  if (document.readyState !== 'loading') {
    fn();
    return;
  }
  document.addEventListener('DOMContentLoaded', fn);
}

export { q, parents, clck, ready }
