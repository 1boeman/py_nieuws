#!/bin/env python
import glob
import re
import os
import datetime
import click
import helpers.util as u
import subprocess
import yaml
import markdown
from jinja2 import Environment, FileSystemLoader


logger = u.get_logger(__file__)


@click.command()
@click.option('--type', default='news', help='news or directory or news_blocks or front')
def main(type):
    _type = type
    public_dir = u.get_setting('public_dir')

    # sync frontend
    sync_frontend(public_dir)
    if _type == 'front':
        # quit after syncing frontend files
        return
    pages = u.get_pages()

    for file_path in pages:
        output = ''
        page_yaml = u.get_page_yaml(file_path)
        if _type == 'news' and page_yaml['type'] == 'news':
            try:
                output = render_news_page(pages[file_path])
            except Exception as e:
                logger.exception(e)
                continue

        elif _type == 'news_blocks'  and page_yaml['type'] == 'news':
            block_dir = os.path.join(public_dir, 'blocks')
            if not os.path.isdir(block_dir):
                os.makedirs(block_dir)
            outputs = render_news_blocks(pages[file_path])
            for o in outputs:
                out_path = os.path.join(block_dir, o)
                write_file(out_path, outputs[o])
        elif _type == 'directory' and page_yaml['type'] == 'directory':
            output = render_directory_page(pages[file_path])

        if len (output):
            out_name = get_out_name(file_path)
            out_path = os.path.join(public_dir,out_name)
            write_file(out_path,output)


def sync_frontend(public_dir):
    www_assets=u.get_setting('www_assets')
    subprocess.call(["rsync", "-avhq", "--delete", www_assets, public_dir])


def write_file(out_path,output):
    with open(out_path,'w') as o:
        o.write(output) 
        click.secho(out_path,fg='green')


def render_directory_page(page):
    public_dir = u.get_setting('public_dir')
    data = {"page_title" : page['title']}
    content_dir = './pages/' + page['dir']
    data['intro'] = convert_markdown_file(content_dir + '/index.md')
    data['menu'] = menu_data()

    out_dir = os.path.join(public_dir,page['dir'])
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    data['sub_menu'] = sub_menu(content_dir, out_dir)
    for md_path in glob.glob(content_dir + '/*.md'):
        if "index.md" not in md_path:
            html_file = os.path.basename(md_path).replace(".md",".html")
            html_path = os.path.join(out_dir, html_file)
            article_page = render_directory_article(md_path)
            write_file(html_path,article_page)

    output = render_page(data,'directory_page.html')
    return output


def sub_menu(content_dir, out_dir):
    public_dir = u.get_setting('public_dir')
    menu_links = {}
    for md_path in glob.glob(content_dir + '/*.md'):
        if "index.md" not in md_path:
            html_file = os.path.basename(md_path).replace(".md",".html")
            html_path = os.path.join(out_dir, html_file)
            link_path = html_path.replace(public_dir, '.')
            _list = get_directory_article_data(md_path)
            data = yaml.safe_load(_list[0])
            article_date = datetime.datetime.strptime(data['date'],'%d/%m/%Y')
            sort_date = article_date.strftime('%Y-%m-%d')
            menu_links[sort_date] = {"link": link_path, "title": data['title']}
    return dict(sorted(menu_links.items(), reverse=True))


def get_directory_article_data(file_path):
    with open(file_path, 'r', encoding='utf8') as file_in:
        return file_in.read().split('----') 


def render_directory_article(file_path):
        _list = get_directory_article_data(file_path)
        data = yaml.safe_load(_list[0])
        data['main'] = convert_markdown(_list[1])
        data['menu'] = menu_data()
        output = render_page(data,'directory_article.html')
        return output


def render_news_page(page):
    data = {"page_title" : page['title'],
            "sections" : []}
    for section in page['content']:
        for source in section:
            try:
                news_block(source)
            except Exception as e:
                logger.exception(e)
                continue
        data["sections"].append(section)
        data['menu'] = menu_data()
    output = render_page(data,'news_page.html')
    return output


def news_block(source):
    print ('**' + source['src'])
    source_data = u.get_data(source['src'])
    if source_data:
        source['data'] = source_data
        source['data']['identifier'] = u.url_to_filename(source['src'])
        source['db_source'] = u.get_source(source['data']['identifier'])
        if source['db_source']['process']:
            source['db_source']['process'] = source['db_source']['process'].split(',')
    else:
        click.secho('no data for ' + source['src'])


def render_news_blocks(page):
    outputs = {}
    for section in page['content']:
        for source in section:
            newsblock(source)
            outputs[source['data']['identifier']] = render_page(source,'news_block.html',True)
    return outputs


def convert_markdown(text):
        return markdown.markdown(text)


def convert_markdown_file(file_path):
    with open(file_path, 'r', encoding='utf8') as file_in:
        text = file_in.read()
        content = convert_markdown(text)
        return content


def get_out_name(yml_path):
    return re.sub(r'\.yml','.html',os.path.basename(yml_path))


def menu_data():
    menu_data = {}
    pages = u.get_pages()
    for file_path in pages:
        menu_data[get_out_name(file_path)] = pages[file_path]['title']
    return menu_data


def render_page(data, tpl_file, news_block=False):
    templates_dir = u.get_dir_setting('templates_dir') 
    file_loader = FileSystemLoader(templates_dir)
    env = Environment(loader=file_loader, autoescape=True)
    template = env.get_template(tpl_file)
    template.globals['now'] = datetime.datetime.utcnow()
    template.globals['local_now'] = datetime.datetime.now()
    if not news_block:
        output = template.render(data = data)
    else:
        output = template.render(source = data)
    return output


if __name__ == "__main__":
    main()
