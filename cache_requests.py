#!/bin/env python
import requests
import os
import shutil
import helpers.util as u


logger = u.get_logger(__file__)


def main():
    l = [   ('sites_dir','sites_file'),
            ('feeds_dir','feeds_file'),
            ('json_dir','json_file')]
    [cache(*p) for p in l]


def cache(dir_key,file_key):
    _dir = u.get_dir_setting(dir_key)
    u.delete_dir_contents(_dir, max_file_age = 60*60*24*2) # 2days
    _file = u.get_setting(file_key)
    if os.path.isfile(_file):
        lines = u.get_file_lines(_file)
        for line in lines:
            try:
                logger.info(line)
                resp = requests.get(line,timeout=30)
                file_name = u.url_to_filename(line)
                src = u.get_source(file_name)
                file_path = os.path.join(_dir, file_name)
                with open (file_path,"wb") as g:
                    g.write(resp.content)
            except Exception as e:
                logger.exception(e)
                continue

if  __name__ == "__main__":
    main()
