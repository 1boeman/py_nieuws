#!/bin/env python
import os
import json
import sys
import glob
import time
import click
import logging
import importlib
import helpers.scrape_util as su
import helpers.util as u
import traceback
import feedparser


logger = u.get_logger(__file__)


@click.command()
@click.option('-s', '--scraper')
def main(scraper):
    if (scraper):
        html_to_data(scraper=scraper)
    else:
        json_to_data()
        html_to_data()
        feeds_to_data()


def feeds_to_data():
    data_dir = u.get_dir_setting('data_dir')
    dir_path = u.get_setting('feeds_dir')

    files = glob.glob(dir_path + '/*')
    for file_path in files:
        #print(file_path)
        file_name = os.path.basename(file_path)
        src = u.get_source(file_name)
        logger.info(src)
        feed_data = feedparser.parse(file_path)
        if feed_data['bozo'] == 1:
            click.secho('Error : bozo feed for:' + file_path, fg='red')
        #    print(feed_data)
        else: 
            src = u.get_source(file_name)
            if not src:
              click.secho('Not in db_file. Skipping! ' + file_name, fg='red')
              continue
            data = su.channel() 
            try:
                data['title'] = src['title']
                try:
                    data['link'] =  feed_data['feed']['link']
                except KeyError as e:
                    data['link'] =  src['url']

                try:
                    data['description']  = feed_data['feed']['description']
                except KeyError as e:
                    data['description'] =  '-'

                for entry in feed_data['entries']:
                    item = su.item()
                    item['title'] = entry['title']
                    item['link'] = entry['link']
                    try:
                        item['description'] = entry['summary']
                    except KeyError as e:
                        item['description'] = '-'
                    try:
                        item['pubDate'] = time.strftime('%Y-%m-%d', entry['updated_parsed']) if not 'published' in entry else entry['published'] 
                    except KeyError as e:
                        item['pubDate'] = ''
                    data['entries'].append(item)
                save_data(file_name, data, src['process'])
            except Exception as e:
                click.secho(e, fg='red')
                click.secho('Error (feeds_to_data) in :' + file_path, fg='red')
                print(traceback.format_exc())
                continue


def json_to_data(scraper=None):
    data_dir = u.get_dir_setting('data_dir')
    dir_path = u.get_setting('json_dir')
    files = glob.glob(dir_path + '/*')
    d = u.get_setting('json_data_maps')
    process_data_maps(dir_path, files, d, scraper)


def html_to_data(scraper=None):
    _dirs = ['selenium_dir','sites_dir']
    for _dir in _dirs:
        dir_path = u.get_setting(_dir)
        files = glob.glob(dir_path + '/*')
        d = u.get_setting('sites_data_maps')
        process_data_maps(dir_path, files, d, scraper)


def process_data_maps(dir_path, files, map_dir, scraper):
    i = 0
    for file_path in files:
     #   print(file_path)
        file_name = os.path.basename(file_path)
        src = u.get_source(file_name)
        if not src:
          click.secho('Not in db_file. Skipping! ' + file_name, fg='red')
          continue
        if scraper and scraper != file_name:
            pass
        else:
            try:
                data = scrape(file_path, file_name, module_dir = map_dir)
            except Exception as e:
                click.secho(e, fg='red')
                click.secho('Error (html_to_data) in :' + file_path, fg='red')
                print(traceback.format_exc())
                continue
            if data:
                i+=1
                try:
                  data['link'] = src['url']
                  save_data(file_name, data, src['process'])
                except Exception as e:
                  click.secho(e, fg='red')
                  click.secho('Error (process_data_maps) in :' + file_path, fg='red')
      #            print(traceback.format_exc())
                  continue

        print (str(i) + ' scraper modules executed')


def save_data(file_name, data, preprocess):
    if "entries" in data:
        if not len(data['entries']):
           click.secho("Warning: " + file_name + " contains 0 entries", fg='red' )
        data_dir = u.get_dir_setting('data_dir')
        save_path = os.path.join(data_dir, file_name)
        if preprocess:
            preprocess = preprocess.split(',')
            if 'reverse' in preprocess:
                data['entries'].reverse()

        with open(save_path,"w") as data_file:
            json.dump(data,data_file, indent=2)
            click.secho(save_path, fg='green')
    else:
        click.secho("Warning: " + file_name + " not processed ", fg='red' )


def scrape(file_path, file_name, module_dir = 'scrapers'):
    data = {}
    data_map = u.get_processing_instructions(file_name, specific="data_map")
    if len(data_map):
        scrape_module = data_map[0].split(':')[1]
    else:
        scrape_module = file_name
    
    module_file_path =  os.path.join(module_dir, scrape_module + '.py')

    sys.path.insert(0, module_dir)
    if os.path.isfile(module_file_path):
        mod = importlib.import_module(scrape_module)
        data = mod.get_data(file_path)
    else :
        click.secho('Warning: missing scraper file ' + module_file_path, fg='red')
    return data


if __name__ == "__main__":
    main()
