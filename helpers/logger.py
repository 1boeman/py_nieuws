import logging
from logging.handlers import RotatingFileHandler


def init_logger(log_file_path=None):
    ch = logging.StreamHandler()
    ch.setFormatter(CustomFormatter())
    _handlers = [ch]
    if log_file_path:
        file_handler = RotatingFileHandler(log_file_path, maxBytes=2000000, backupCount=10)
        _handlers.append(file_handler)

    logging.basicConfig(handlers=_handlers, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', 
                            level=logging.DEBUG)


def logger_child(name, log_file_path):
    if not logging.getLogger().hasHandlers():
        init_logger(log_file_path)

    logger = logging.getLogger(name)
    return logger


class CustomFormatter(logging.Formatter):
    grey = "\x1b[38;20m"
    blue = "\x1b[34;20m"
    green = "\x1b[32;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"

    FORMATS = {
        logging.DEBUG: blue + format + reset,
        logging.INFO: grey + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)
