from bs4 import BeautifulSoup
from lxml import etree


def channel():
    return {
        "title":"",
        "link":"",
        "description":"",
        "entries":[]
    } 


def item():
    return {
        "title":"",
        "link":"",
        "description":"",
        "pubDate":"",
        "enclosure":{
            "url":"",
            "type":"",
        }
    }


def soup_from_file(file_path):
    with open (file_path) as fp:
        soup = BeautifulSoup(fp)
    return soup


def lx_etree (file_path):
    htmlparser = etree.HTMLParser(encoding='utf-8')
    tree = etree.parse(file_path, htmlparser)
    return tree


def fix_link(path_or_link, web_root):
    if path_or_link[0:4].lower() == 'http':
        return path_or_link
    return web_root + path_or_link

def lx_text_all(node_list):
    text_list = []
    for node in node_list:
        text_list.append (' || '.join(node.itertext()).strip())
    return " || ".join(text_list)


def lx_text(node):
    return ' || '.join(node.itertext()).strip()
