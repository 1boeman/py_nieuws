import re
import os
import click
import json
import glob
import os
import yaml
import time
import sqlite3 as sql3
from helpers.logger import logger_child
from yaml.loader import SafeLoader


def get_setting(key):
    ''' retrieve value from config file '''
    config_file_path = os.getenv('PYNIEUWS_CONFIG',"./config.yaml")

    with open(config_file_path) as f:
        config_data = list(yaml.load_all(f, Loader=SafeLoader))[0]
        if key in config_data:
            return config_data[key]
        else:
            raise Exception(key + " not set")
    return None


def get_logger(name):
    return logger_child(name, log_file_path = get_setting('log_file_path'))


def get_data(src_url):
    data_dir = get_setting('data_dir')
    file_name = url_to_filename(src_url)
    file_path = os.path.join(data_dir, file_name)
    if os.path.isfile(file_path):
        with open(file_path) as json_file:
            file_contents = json_file.read()
            parsed_json = json.loads(file_contents)
            return parsed_json
    else:
        click.secho('Warning: missing data file for ' + src_url, fg='red')
        return {}


def get_source(data_file_name):
    db_file = get_setting('db_file')
    conn = sql3.connect(db_file)
    conn.row_factory = sql3.Row
    c = conn.cursor()
    sql = ''' select * from sources where data_file = ? '''
    c.execute(sql, (data_file_name, ))
    try:
        result = dict(c.fetchone())
    except Exception as e:
        click.secho(e, fg='red')
        result = {}
    conn.close()
    return result


def get_processing_instructions(data_file_name, specific=""):
    db_record = get_source(data_file_name)
    if db_record['process']:
        instructions = db_record['process'].split(',')
        if len(specific):
            return [instr for instr in instructions if specific in instr] 
        return instructions
    else:
        return []


def get_file_lines(path):
    ''' retrieve content of file as list of lines '''
    lines = []
    with open(path) as f:
        lines = f.read().strip().splitlines()
    return lines


def url_to_filename(url):
    file_name = re.sub('[^0-9a-zA-Z]+', '_', url)
    return file_name


def get_dir_setting(key):
    path = get_setting(key)
    if not os.path.isdir(path):
        os.makedirs(path)
    return path


def get_setting(key):
    ''' retrieve value from config file '''
    config_file_path = os.getenv('PYNIEUWS_CONFIG',"./config.yaml")

    with open(config_file_path) as f:
        config_data = list(yaml.load_all(f, Loader=SafeLoader))[0]
        if key in config_data:
            return config_data[key]
        else:
            raise Exception(key + " not set")
    return None


def delete_dir_contents(dir_path, max_file_age=0):
    filelist = glob.glob(os.path.join(dir_path, "*"))
    now_ts = time.time()
    for f in filelist:
        try:
          if not max_file_age:
              os.remove(f)
          else:
              create_ts = os.path.getctime(f)
              ts_diff = now_ts - create_ts
              if ts_diff > max_file_age:
                  os.remove(f)
        except Exception as e:
            logger = get_logger('util')
            logger.exception(e)
            continue

def get_page_yaml(path):
    with open(path, 'r') as f:
        page_data = list(yaml.load_all(f, Loader=SafeLoader))
    return page_data[0]


def get_pages():
    ''' retrieve yaml page representations as dict '''
    pages_dir = get_dir_setting('pages_dir')
    pages = {}
    for path in glob.glob(os.path.join(pages_dir, '*.yml')):
        pages[path] = get_page_yaml(path)
    if not pages:
        logger.error("pages_dir contains 0 yaml files:" + pages_dir  )
    return pages
