# syntax=docker/dockerfile:1

FROM python:3.11-alpine

RUN apk update && \
    apk upgrade


RUN apk add --no-cache build-base gcc musl-dev python3-dev libffi-dev libxml2-dev libxslt-dev bash rsync

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install "cython<3.0.0" pyyaml==5.4.1 --no-build-isolation -v
RUN pip install -r requirements.txt

COPY . .
